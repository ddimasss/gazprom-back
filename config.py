from pydantic import BaseSettings
import os


class Settings(BaseSettings):
    APP_NAME: str = 'gazprom'
    URL_PREFIX_V1: str = '/api/v1'
    PORT: int = 4000
    ENV: str = 'prod'
    DB_PORT: int = 5432
    DB_HOST: str = '188.168.35.10'
    DB_USER: str = 'ddimasss'
    DB_PASS: str = 'Serial111'
    DB_DATABASE: str = 'gazprom'
    CLOUD_PATH: str =  os.path.join(os.getcwd(),'models')

    class Config:
        def __init__(self):
            pass

        env_prefix = 'gaz_'


settings = Settings()