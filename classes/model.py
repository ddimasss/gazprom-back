from typing import Optional
from pydantic import BaseModel
from datetime import date

class Model(BaseModel):
    id: Optional[int] = None
    project_id: Optional[int] = None
    caption: str
    created_at: Optional[date] = date.today()
    updated_at: Optional[date] = date.today()
    is_deleted: bool = False
    status: Optional[int] = 3
    version: str
    last_test: Optional[date] = None
    data_weight: Optional[int] = None