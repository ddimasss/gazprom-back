from typing import Optional
from pydantic import BaseModel
from datetime import date

class Project(BaseModel):
    id: Optional[int] = None
    description: Optional[str] = None
    caption: str
    created_at: Optional[date] = date.today()
    updated_at: Optional[date] = date.today()
    is_deleted: bool = False
    status: Optional[int] = 1
    model_type: Optional[int] = 1
    employee: Optional[str] = None
    employee_contact: Optional[str] = None
    target_error: bool = False
    target_value: int
    target_caption: str