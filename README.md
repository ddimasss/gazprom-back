Для запуска API необходимо:
1. Установить на сервере python версии 3.7 или выше.
2. Создать виртуальную среду разработки и запустить ее.  
python -m venv venv  
source ./venv/bin/activate
3. Установить необходимые библиотеки для работы python-API внутри виртуальной среды из файла **requirements.txt**.  
pip install -r requirements.txt
4. Проверить и настроить файл конфигурации **config.py**.
5. Запустить python-API.  
uvicorn main:app

