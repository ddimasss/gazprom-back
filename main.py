from fastapi import FastAPI, Depends, Header, HTTPException, status, Response, Request
from typing import Optional
import uvicorn
from starlette.middleware.cors import CORSMiddleware
import psycopg2
from psycopg2.extras import DictCursor, RealDictCursor
import json, uuid, os
from datetime import datetime, timedelta, date

from config import settings
from classes.project import Project
from classes.user import User, Token
from classes.model import Model
# from app.api.api_v1.urls import api_v1_router
# from app.scikit_model.scoring_ml_module import ScoreModel


app = FastAPI()
app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_methods=["*"],
    allow_credentials=True,
    allow_headers=["*"]
)


@app.on_event("startup")
async def startup_event():
    """ Loading ML model """
    # Connect to postgres

    global model
    print('Start loading ML model')
#    ScoreModel()
    print('Done')


def verify_token(token: str):
    """ Проверка авторизации пользователя по токену

        :param token: токен авторизации из заголовка Authorization

        :return: True - авторизация успешна
    """
    with psycopg2.connect(dbname=settings.DB_DATABASE, user=settings.DB_USER,
                            password=settings.DB_PASS, host=settings.DB_HOST) as conn:
        with conn.cursor(cursor_factory=RealDictCursor) as cursor:
            cursor.execute(f'''
                SELECT id
                FROM users
                WHERE token='{token}'
            ''')
            user_id = cursor.fetchone()
            if user_id:
                return True
    raise HTTPException(
        status_code=status.HTTP_401_UNAUTHORIZED,
        detail="Unauthorized",
    )

def get_user_profile_by_id(id: int):
    """ Получение данных пользователя по id

        :param id: id пользователя

        :return: профайл пользователя из БД (dict)
    """
    user_profile = {}
    with psycopg2.connect(dbname=settings.DB_DATABASE, user=settings.DB_USER,
                            password=settings.DB_PASS, host=settings.DB_HOST) as conn:
        with conn.cursor(cursor_factory=RealDictCursor) as cursor:
            cursor.execute(f'''
                SELECT "group",first_name,last_name,middle_name,birth_day,sex,id,created_at,updated_at
                FROM profile
                WHERE id={id}
            ''')
            user_profile = cursor.fetchone()
            if not user_profile:
                user_profile = {}

            cursor.execute(f'''
                SELECT id, status, type, value, created_at, updated_at
                FROM contacts
                WHERE user_id={id}
            ''')
            user_profile['contact'] = cursor.fetchall()

    return user_profile

############################ Поинты авторизации ################################
@app.post(settings.URL_PREFIX_V1 + "/login")
async def login(user: User):
    with psycopg2.connect(dbname=settings.DB_DATABASE, user=settings.DB_USER,
                            password=settings.DB_PASS, host=settings.DB_HOST) as conn:
        with conn.cursor(cursor_factory=RealDictCursor) as cursor:
            cursor.execute(f'''
                SELECT id
                FROM users
                WHERE username='{user.username}' and password='{user.password}'
            ''')
            user_profile = cursor.fetchone()
            if not user_profile:
                raise HTTPException(
                    status_code=status.HTTP_401_UNAUTHORIZED,
                    detail="Invalid authentication credentials",
                )

            user_profile['refreshToken'] = str(uuid.uuid4().hex)
            user_profile['token'] = str(uuid.uuid4().hex)
            data = {
                'data':{
                    'person':get_user_profile_by_id(user_profile['id']),
                    'token':user_profile['token'],
                    'refreshToken':user_profile['refreshToken']
                }
            }

            token_timeout = datetime.now() + timedelta(hours=24)
            cursor.execute('''
                UPDATE users
                SET "refreshToken"='{refresh_token}',
                    "token_timeout"='{timeout}',
                    "token"='{token}'
                WHERE id={user_id}
            '''.format(
                refresh_token = user_profile['refreshToken'],
                timeout = token_timeout.strftime('%Y-%m-%d %H:%M:%S'),
                token = user_profile['token'],
                user_id = user_profile['id']
            ))

    return data

@app.post(settings.URL_PREFIX_V1 + "/refresh")
async def refresh(token: Token):
    with psycopg2.connect(dbname=settings.DB_DATABASE, user=settings.DB_USER,
                            password=settings.DB_PASS, host=settings.DB_HOST) as conn:
        with conn.cursor(cursor_factory=RealDictCursor) as cursor:
            cursor.execute(f'''
                SELECT id
                FROM users
                WHERE "refreshToken"='{token.refreshToken}'
            ''')
            user = cursor.fetchone()

            if user:
                refresh_token = str(uuid.uuid4().hex)
                token = str(uuid.uuid4().hex)
                data = {
                    'data':{
                        'person':get_user_profile_by_id(user['id']),
                        'token':token,
                        'refreshToken':refresh_token
                    }
                }

                token_timeout = datetime.now() + timedelta(hours=1)
                cursor.execute('''
                    UPDATE users
                    SET "refreshToken"='{refresh_token}',
                        "token_timeout"='{timeout}',
                        "token"='{token}'
                    WHERE id={user_id}
                '''.format(
                    refresh_token = refresh_token,
                    timeout = token_timeout.strftime('%Y-%m-%d %H:%M:%S'),
                    token = token,
                    user_id = user['id']
                ))

                return data

    raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Unauthorized",
        )

@app.post(settings.URL_PREFIX_V1 + "/logout")
async def logout(authorization: Optional[str] = Header(None)):
    result = False
    if verify_token(authorization):
        with psycopg2.connect(dbname=settings.DB_DATABASE, user=settings.DB_USER,
                                password=settings.DB_PASS, host=settings.DB_HOST) as conn:
            with conn.cursor(cursor_factory=RealDictCursor) as cursor:
                cursor.execute(f'''
                    SELECT id
                    FROM users
                    WHERE "token"='{authorization}'
                ''')
                user = cursor.fetchone()

                cursor.execute('''
                    UPDATE users
                    SET "refreshToken"=null,
                        token_timeout=null,
                        "token"=null
                    WHERE id={user_id}
                    RETURNING id
                '''.format(user_id=user['id']))
                record = cursor.fetchone()
                if record:
                    result = True

    return {"data": {"result": result} }
########################## end Поинты авторизации ##############################

########################## Поинты работы с проектом ############################
@app.get(settings.URL_PREFIX_V1 + "/projects")
async def projects(authorization: Optional[str] = Header(None),
                   caption: Optional[str] = None,
                   created_at: Optional[date] = None,
                   is_deleted: Optional[bool] = False,
                   status: Optional[int] = -1,
                   model_type: Optional[int] = -1
    ):
    if verify_token(authorization):
        with psycopg2.connect(dbname=settings.DB_DATABASE, user=settings.DB_USER,
                                password=settings.DB_PASS, host=settings.DB_HOST) as conn:
            with conn.cursor(cursor_factory=RealDictCursor) as cursor:
                condition = f'WHERE projects.is_deleted={is_deleted}'
                if status != -1:
                    condition += f" AND projects.status={status}"
                if model_type != -1:
                    condition += f" AND projects.model_type={model_type}"
                if caption:
                    condition += f" AND projects.caption='{caption}'"
                if created_at:
                    condition += f" AND projects.created_at='{created_at}'"

                cursor.execute(f'''
                    SELECT projects.*, ver.ver_model
                    FROM
                    (
                        SELECT project_id AS id, array_agg(models."version") AS ver_model
                        FROM models
                        GROUP BY project_id
                    ) AS ver
                    RIGHT JOIN projects ON projects.id=ver.id
                    {condition}
                    ORDER BY projects.id
                ''')
                records = cursor.fetchall()
        return {"data": records}


@app.get(settings.URL_PREFIX_V1 + "/projects/{id}")
async def projects_by_id(id: int, authorization: Optional[str] = Header(None)):
    if verify_token(authorization):
        with psycopg2.connect(dbname=settings.DB_DATABASE, user=settings.DB_USER,
                                password=settings.DB_PASS, host=settings.DB_HOST) as conn:
            with conn.cursor(cursor_factory=RealDictCursor) as cursor:
                cursor.execute(f'''
                    SELECT projects.*, ver.ver_model
                    FROM
                    (
                        SELECT project_id AS id, array_agg(models."version") AS ver_model
                        FROM models
                        GROUP BY project_id
                    ) AS ver
                    RIGHT JOIN projects ON projects.id=ver.id
                    WHERE projects.id={id}
                ''')
                record = cursor.fetchone()
        return {"data": record}

@app.post(settings.URL_PREFIX_V1 + "/projects")
async def create_project(project: Project, authorization: Optional[str] = Header(None)):
    if verify_token(authorization):
        with psycopg2.connect(dbname=settings.DB_DATABASE, user=settings.DB_USER,
                                password=settings.DB_PASS, host=settings.DB_HOST) as conn:
            with conn.cursor(cursor_factory=RealDictCursor) as cursor:
                cursor.execute(f'''
                    INSERT INTO projects (description, caption, created_at, updated_at, is_deleted, status, model_type, employee, employee_contact, target_value, target_caption)
                        VALUES
                        (
                            '{project.description}',
                            '{project.caption}',
                            '{project.created_at}',
                            '{project.updated_at}',
                            '{project.is_deleted}',
                            {project.status},
                            {project.model_type},
                            '{project.employee}',
                            '{project.employee_contact}',
                            {project.target_value},
                            '{project.target_caption}'
                        )
                        RETURNING id, description, caption, created_at, updated_at, is_deleted,
                                  status, model_type, employee, employee_contact, target_value, target_caption
                ''')
                record = cursor.fetchone()

                if record:
                    cursor.execute('''
                        SELECT array_agg("version") AS ver_model
                        FROM models
                        WHERE project_id={id}
                    '''.format(id=record['id']))
                    models = cursor.fetchone()
                    record['ver_model'] = models['ver_model'] if models else None

        return {"data": record}

@app.put(settings.URL_PREFIX_V1 + "/projects/{id}")
async def update_project(id:int, project: Project, authorization: Optional[str] = Header(None)):
    if verify_token(authorization):
        with psycopg2.connect(dbname=settings.DB_DATABASE, user=settings.DB_USER,
                              password=settings.DB_PASS, host=settings.DB_HOST) as conn:
            with conn.cursor(cursor_factory=RealDictCursor) as cursor:
                cursor.execute(f'''
                    UPDATE projects
                    SET description='{project.description}',
                        caption='{project.caption}',
                        updated_at='{project.updated_at}',
                        status={project.status},
                        model_type={project.model_type},
                        employee='{project.employee}',
                        employee_contact='{project.employee_contact}',
                        target_value={project.target_value},
                        target_caption='{project.target_caption}'
                    WHERE id={id} AND is_deleted=false
                    RETURNING id, description, caption, created_at, updated_at, is_deleted,
                              status, model_type, employee, employee_contact, target_error,
                              target_value, target_caption
                ''')
                record = cursor.fetchone()
                if not record:
                    raise HTTPException(
                        status_code=500,
                        detail="Project not found",
                    )

                cursor.execute(f'''
                    SELECT array_agg("version") AS ver_model
                    FROM models
                    WHERE project_id='{id}'
                ''')
                models = cursor.fetchone()
                record['ver_model'] = models['ver_model'] if models else None

        return {"data": record}

@app.delete(settings.URL_PREFIX_V1 + "/projects/{id}")
async def delete_project(id:int, authorization: Optional[str] = Header(None)):
    if verify_token(authorization):
        with psycopg2.connect(dbname=settings.DB_DATABASE, user=settings.DB_USER,
                                password=settings.DB_PASS, host=settings.DB_HOST) as conn:
            with conn.cursor(cursor_factory=RealDictCursor) as cursor:
                cursor.execute(f'''
                    UPDATE projects
                    SET is_deleted=true
                    WHERE id={id} AND is_deleted=false
                    RETURNING id, description, caption, created_at, updated_at, is_deleted,
                              status, model_type, employee, employee_contact, target_error, target_value, target_caption
                ''')
                record = cursor.fetchone()
                if not record:
                    raise HTTPException(
                        status_code=500,
                        detail="Project not found",
                    )

                cursor.execute(f'''
                    SELECT array_agg("version") AS ver_model
                    FROM models
                    WHERE project_id='{id}'
                ''')
                models = cursor.fetchone()
                record['ver_model'] = models['ver_model'] if models else None

        return {"data": record}
######################## end Поинты работы с проектом ##########################


########################## Поинты работы с моделью #############################
@app.get(settings.URL_PREFIX_V1 + "/model")
async def models(authorization: Optional[str] = Header(None),
                 status: Optional[int] = -1,
                 project_id: Optional[int] = -1,
                 is_deleted: Optional[bool] = False
    ):
    if verify_token(authorization):
        with psycopg2.connect(dbname=settings.DB_DATABASE, user=settings.DB_USER,
                                password=settings.DB_PASS, host=settings.DB_HOST) as conn:
            with conn.cursor(cursor_factory=RealDictCursor) as cursor:
                condition = f'WHERE models.is_deleted={is_deleted}'
                if status != -1:
                    condition += f" AND models.status={status}"
                if project_id != -1:
                    condition += f" AND models.project_id={project_id}"

                cursor.execute(f'''
                    SELECT *
                    FROM models
                    {condition}
                    ORDER BY models.id
                ''')
                records = cursor.fetchall()
        return {"data": records}

@app.get(settings.URL_PREFIX_V1 + "/model/{id}")
async def models_by_id(id: int, authorization: Optional[str] = Header(None)):
    if verify_token(authorization):
        with psycopg2.connect(dbname=settings.DB_DATABASE, user=settings.DB_USER,
                                password=settings.DB_PASS, host=settings.DB_HOST) as conn:
            with conn.cursor(cursor_factory=RealDictCursor) as cursor:
                cursor.execute(f'''
                    SELECT *
                    FROM models
                    WHERE models.id={id}
                ''')
                record = cursor.fetchone()
        return {"data": record}

@app.post(settings.URL_PREFIX_V1 + "/model")
async def create_model(model: Model, authorization: Optional[str] = Header(None)):
    if verify_token(authorization):
        with psycopg2.connect(dbname=settings.DB_DATABASE, user=settings.DB_USER,
                                password=settings.DB_PASS, host=settings.DB_HOST) as conn:
            with conn.cursor(cursor_factory=RealDictCursor) as cursor:
                cursor.execute(f'''
                    INSERT INTO models (project_id, caption, status, version)
                        VALUES
                        (
                            {model.project_id},
                            '{model.caption}',
                            {model.status},
                            '{model.version}'
                        )
                        RETURNING id, project_id, caption, created_at, updated_at, is_deleted,
                                  status, version, last_test, data_weight
                ''')
                record = cursor.fetchone()

        return {"data": record}

@app.put(settings.URL_PREFIX_V1 + "/model/{id}")
async def update_model(id:int, model: Model, authorization: Optional[str] = Header(None)):
    if verify_token(authorization):
        with psycopg2.connect(dbname=settings.DB_DATABASE, user=settings.DB_USER,
                              password=settings.DB_PASS, host=settings.DB_HOST) as conn:
            with conn.cursor(cursor_factory=RealDictCursor) as cursor:
                cursor.execute(f'''
                    UPDATE models
                    SET project_id={model.project_id},
                        caption='{model.caption}',
                        updated_at='{model.updated_at}',
                        status={model.status},
                        version='{model.version}'
                    WHERE id={id} AND is_deleted=false
                    RETURNING id, project_id, caption, created_at, updated_at, is_deleted,
                              status, version, last_test, data_weight
                ''')
                record = cursor.fetchone()
                if not record:
                    raise HTTPException(
                        status_code=500,
                        detail="Model not found",
                    )

        return {"data": record}

@app.delete(settings.URL_PREFIX_V1 + "/model/{id}")
async def delete_model(id:int, authorization: Optional[str] = Header(None)):
    if verify_token(authorization):
        with psycopg2.connect(dbname=settings.DB_DATABASE, user=settings.DB_USER,
                                password=settings.DB_PASS, host=settings.DB_HOST) as conn:
            with conn.cursor(cursor_factory=RealDictCursor) as cursor:
                cursor.execute(f'''
                    UPDATE models
                    SET is_deleted=true
                    WHERE id={id} AND is_deleted=false
                    RETURNING id, project_id, caption, created_at, updated_at, is_deleted,
                              status, version, last_test, data_weight
                ''')
                record = cursor.fetchone()
                if not record:
                    raise HTTPException(
                        status_code=500,
                        detail="Model not found",
                    )

        return {"data": record}

@app.get(settings.URL_PREFIX_V1 + "/files/model/{project_id}")
async def models_files_for_project(project_id: int, authorization: Optional[str] = Header(None)):
    if verify_token(authorization):
        try:
            files = os.listdir(os.path.join(settings.CLOUD_PATH,str(project_id)))
        except Exception:
            raise HTTPException(
                status_code=500,
                detail="ML file for project not found",
            )
        versions = list(os.path.splitext(file)[0] for file in files)

        with psycopg2.connect(dbname=settings.DB_DATABASE, user=settings.DB_USER,
                                password=settings.DB_PASS, host=settings.DB_HOST) as conn:
            with conn.cursor(cursor_factory=RealDictCursor) as cursor:
                cursor.execute(f'''
                    SELECT array_agg("version") as versions
                    FROM models
                    WHERE models.project_id={project_id}
                      AND is_deleted=false
                ''')
                models_versions = cursor.fetchone()
                if models_versions['versions']:
                    versions = list(set(versions) - set(models_versions['versions']))

        return {"data": versions}

######################## end Поинты работы с моделью ###########################


########################## Поинты работы с графиками ###########################
@app.get(settings.URL_PREFIX_V1 + "/graph/roc_auc")
async def roc_auc_for_project(project_id: int, authorization: Optional[str] = Header(None)):
    if verify_token(authorization):
        with psycopg2.connect(dbname=settings.DB_DATABASE, user=settings.DB_USER,
                                password=settings.DB_PASS, host=settings.DB_HOST) as conn:
            with conn.cursor(cursor_factory=RealDictCursor) as cursor:
                cursor.execute(f'''
                    SELECT category, first, second, third
                    FROM roc_auc
                    WHERE roc_auc.project_id={project_id}
                ''')
                metrics = cursor.fetchall()

        return {"data": metrics}

@app.get(settings.URL_PREFIX_V1 + "/graph/forecast_accuracy")
async def forecast_accuracy_for_project(project_id: int, authorization: Optional[str] = Header(None)):
    if verify_token(authorization):
        with psycopg2.connect(dbname=settings.DB_DATABASE, user=settings.DB_USER,
                                password=settings.DB_PASS, host=settings.DB_HOST) as conn:
            with conn.cursor(cursor_factory=RealDictCursor) as cursor:
                cursor.execute(f'''
                    SELECT category, first, second, third
                    FROM forecast_accuracy
                    WHERE forecast_accuracy.project_id={project_id}
                ''')
                metrics = cursor.fetchall()

        return {"data": metrics}

@app.get(settings.URL_PREFIX_V1 + "/graph/customer_churn")
async def customer_churn_for_project(project_id: int, authorization: Optional[str] = Header(None)):
    if verify_token(authorization):
        with psycopg2.connect(dbname=settings.DB_DATABASE, user=settings.DB_USER,
                                password=settings.DB_PASS, host=settings.DB_HOST) as conn:
            with conn.cursor(cursor_factory=RealDictCursor) as cursor:
                cursor.execute(f'''
                    SELECT category, first, second, third
                    FROM customer_churn
                    WHERE customer_churn.project_id={project_id}
                ''')
                metrics = cursor.fetchall()

        return {"data": metrics}

@app.get(settings.URL_PREFIX_V1 + "/dashboard")
async def dashboard(authorization: Optional[str] = Header(None)):
    if verify_token(authorization):
        with open('dashboard_data.json','r', encoding='utf-8') as file:
            data = json.load(file, encoding='utf-8')
            return data
####################### end Поинты работы с графиками ##########################


if __name__ == "__main__":
    uvicorn.run("main:app", host="0.0.0.0", port=settings.PORT, log_level="info", reload=False)
